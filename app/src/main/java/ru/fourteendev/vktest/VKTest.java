package ru.fourteendev.vktest;

import android.app.Application;

import com.vk.sdk.VKSdk;

/**
 * Created by kid on 10.04.2016.
 */
public class VKTest extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        VKSdk.initialize(this);
    }
}
